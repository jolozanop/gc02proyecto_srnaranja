package com.example.asee_gps_pokepin.auth;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.asee_gps_pokepin.models.User;
import com.example.asee_gps_pokepin.data.repository.UserRepository;

public class UserViewModel extends AndroidViewModel {
    private UserRepository userRepository;

    private LiveData<User> userLiveData;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = UserRepository.getInstance();
        userLiveData = null;
    }

    public Boolean signInWithEmailAndPassword(String email, String password) {
        boolean aux = false;
        if (userRepository.signInWithEmailAndPassword(email, password)){
            userLiveData = userRepository.getCurrentUser();
            aux=true;
        }
        return aux;
    }

    public Boolean createUserWithEmailAndPassword(String email, String password, User u){
        boolean aux = false;
        if (userRepository.createUserWithEmailAndPassword(email, password, u)){
            userLiveData = userRepository.getCurrentUser();
            aux=true;
        }
        return aux;
    }

    public LiveData<User> getCurrentUser(){
        userLiveData = userRepository.getCurrentUser();
        return userLiveData;
    }

    public void signOut(){
        userRepository.signOut();
    }
}
