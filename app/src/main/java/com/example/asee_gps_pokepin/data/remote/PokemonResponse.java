package com.example.asee_gps_pokepin.data.remote;

import com.example.asee_gps_pokepin.models.Pokemon;

import java.util.ArrayList;

public class PokemonResponse {

    private ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    @SuppressWarnings("unused")
    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
