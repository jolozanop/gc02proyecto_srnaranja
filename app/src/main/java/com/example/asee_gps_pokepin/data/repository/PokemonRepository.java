package com.example.asee_gps_pokepin.data.repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.data.local.AppDatabase;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.data.remote.PokemonResponse;
import com.example.asee_gps_pokepin.data.remote.RetrofitClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PokemonRepository {

    private Application application;

    private static PokemonRepository mInstance;

    private RetrofitClient retrofit = RetrofitClient.getInstance();

    private AppDatabase database;

    private static MutableLiveData<List<Pokemon>> allPokemonData = new MutableLiveData<>();


    public static synchronized PokemonRepository getInstance(Application application){
        if(mInstance == null){
            mInstance = new PokemonRepository(application);
        }
        return mInstance;
    }

    private PokemonRepository(Application application) {
        database = AppDatabase.getDatabase(application);
        this.application = application;
    }

    public void loadAllPokemon() {
        int offset = application.getResources().getInteger(R.integer.offset);
        int limit = application.getResources().getInteger(R.integer.limit);
        Call<PokemonResponse> pokemonResponseCall = retrofit.getApi().getListaPokemon(limit, offset);

        pokemonResponseCall.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PokemonResponse> call, @NonNull Response<PokemonResponse> response) {
                if (response.isSuccessful()) {
                    PokemonResponse pokemonResponse = response.body();
                    assert pokemonResponse != null;
                    ArrayList<Pokemon> listPokemon = pokemonResponse.getResults();
                    allPokemonData.setValue(listPokemon);
                    for (Pokemon p : listPokemon) {
                        Call<Pokemon> pokemonCall = retrofit.getApi().getPokemonByName(p.getName());
                        pokemonCall.enqueue(new Callback<Pokemon>() {
                            @Override
                            public void onResponse(@NonNull Call<Pokemon> call, @NonNull Response<Pokemon> response) {
                                assert response.body() != null;
                                Objects.requireNonNull(allPokemonData.getValue()).set(response.body().getId() - 1, response.body());
                                insertPokemon(new MutableLiveData<>(response.body()));
                            }

                            @SuppressWarnings("NullableProblems")
                            @Override
                            public void onFailure(Call<Pokemon> call, Throwable t) {
                                throw new UnsupportedOperationException();
                            }
                        });
                    }
                    allPokemonData.setValue(allPokemonData.getValue());
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonResponse> call, @NonNull Throwable t) {
                throw new UnsupportedOperationException();
            }
        });
    }

    public LiveData<List<Pokemon>> getAllPokemon() {
        return database.pokemonDao().getAll();
    }


    private void insertPokemon(LiveData<Pokemon> pokemon) {
        AppDatabase.databaseWriteExecutor.execute(() -> database.pokemonDao().insert(pokemon.getValue()));
    }

    public LiveData<List<Pokemon>> searchByName(String s){
        return database.pokemonDao().getAllByName(s);
    }

    public LiveData<Pokemon> searchSingleByName(String s){
        return database.pokemonDao().getByName(s);
    }


}

