package com.example.asee_gps_pokepin.data.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.models.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class UserRepository {

    private DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");

    private MutableLiveData<User> user = new MutableLiveData<>();

    private static UserRepository instance;


    public static synchronized UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    public LiveData<User> getCurrentUser() {
        ref.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user.postValue(dataSnapshot.getValue(User.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return user;
    }

    LiveData<String> getCurrentUserUid() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        assert user != null;
        return new MutableLiveData<>(user.getUid());
    }


    public Boolean signInWithEmailAndPassword(String email, String password) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() == null) {
            // [START sign_in_with_email]
            // Sign in success, update UI with the signed-in user's information
            // If sign in fails, display a message to the user.
            mAuth.signInWithEmailAndPassword(email.toLowerCase(), password)
                    .addOnCompleteListener(Task::isSuccessful);
            // [END sign_in_with_email]
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (mAuth.getCurrentUser() == null)
                return false;
            else {
                getCurrentUser();
                return true;
            }
        } else {
            return true;
        }
    }

    public Boolean createUserWithEmailAndPassword(String email, String password, User u) {
        //create user
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email.toLowerCase(), password)
                .addOnCompleteListener(task -> {
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (task.isSuccessful()) {
                        //Using the UID for storing data into the database
                        ref.child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid()).setValue(u);
                    }
                });
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (mAuth.getCurrentUser() == null)
            return false;
        else {
            getCurrentUser();
            return true;
        }
    }

    public void signOut() {
        FirebaseAuth.getInstance().signOut();
    }
}
