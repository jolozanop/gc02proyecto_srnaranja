package com.example.asee_gps_pokepin.data.repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.data.local.AppDatabase;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class PokemonFavRepository {

    private AppDatabase database;

    private FirebaseAuth auth = FirebaseAuth.getInstance();

    private final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users").child(Objects.requireNonNull(auth.getCurrentUser()).getUid()).child("Fav");

    private static PokemonFavRepository mInstance;

    private MutableLiveData<List<Pokemon>> pokemonFavList = new MutableLiveData<>();

    private MutableLiveData<Boolean> isPokemonFav = new MutableLiveData<>();

    private PokemonFavRepository(Application application) {
        database = AppDatabase.getDatabase(application);
        getPokemonFav();
    }

    public static synchronized PokemonFavRepository getInstance(Application application) {
        if (mInstance == null) {
            mInstance = new PokemonFavRepository(application);
        }
        return mInstance;
    }

    private void addPokemonFav(Pokemon p, ArrayList<Pokemon> aux) {
        aux.add(p);
        aux.sort((o1, o2) -> o1.getId().compareTo(o2.getId()));
        pokemonFavList.postValue(aux);
    }

    public MutableLiveData<List<Pokemon>> getPokemonFav() {
        pokemonFavList.postValue(new ArrayList<>());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    ArrayList<Pokemon> aux = new ArrayList<>();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Integer pokemonId = ds.getValue(Integer.class);
                        if (pokemonId != null)
                            database.pokemonDao().getPokemon(pokemonId).observeForever(pokemon -> addPokemonFav(pokemon, aux));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return pokemonFavList;
    }

    public void switchFavourite (Integer idPokemon) {
        Query q = ref.orderByValue().equalTo(idPokemon);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    ref.child(idPokemon.toString()).setValue(idPokemon);
                }
                else{
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        ds.getRef().removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public LiveData<Boolean> isFavourite(Integer idPokemon) {
        Query q = ref.orderByValue().equalTo(idPokemon);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    isPokemonFav.postValue(Boolean.FALSE);
                } else{
                    isPokemonFav.postValue(Boolean.TRUE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return isPokemonFav;
    }
}
