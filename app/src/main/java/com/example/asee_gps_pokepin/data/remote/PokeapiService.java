package com.example.asee_gps_pokepin.data.remote;

import com.example.asee_gps_pokepin.models.Pokemon;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PokeapiService {

    @GET("pokemon")
    Call<PokemonResponse>getListaPokemon(@Query("limit")int limit, @Query("offset")int offset);

    @GET("pokemon/{name}")
    Call<Pokemon> getPokemonByName(@Path("name") String name);
}