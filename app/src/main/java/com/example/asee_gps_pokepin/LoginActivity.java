package com.example.asee_gps_pokepin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.asee_gps_pokepin.auth.UserViewModel;
import com.example.asee_gps_pokepin.data.repository.PokemonRepository;
import com.example.asee_gps_pokepin.ui.NavigationDrawerActivity;

import java.util.Locale;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEditText;
    private EditText passwordEditText;

    private UserViewModel userViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        String languageCode = getSharedPreferences("settings", Context.MODE_PRIVATE).getString("language", "es");
        if (!languageCode.equals(Locale.getDefault().getLanguage()))
            putLanguage(languageCode);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).hide();

        usernameEditText = findViewById(R.id.signin_username);
        passwordEditText = findViewById(R.id.signin_password);


        String user = readValue("user");
        String password = readValue("password");

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        if (user != null && !user.equals("") && password != null && !password.equals("")) {
           signIn(true, user, password);
        } else {
            PokemonRepository.getInstance(getApplication()).loadAllPokemon();
        }

    }

    private boolean validateForm() {
        boolean valid = true;

        String email = usernameEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            usernameEditText.setError("Required.");
            valid = false;
        } else {
            usernameEditText.setError(null);
        }

        String password = passwordEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError("Required.");
            valid = false;
        } else {
            passwordEditText.setError(null);
        }

        return valid;
    }

    private void signIn(boolean logged, final String email, final String password) {
        if (!logged && !validateForm()) {
            return;
        }

        // [START sign_in_with_email]
        if (userViewModel.signInWithEmailAndPassword(email, password)){
            saveValue("user",email);
            saveValue("password",password);

            finishAffinity();
            startActivity(new Intent(LoginActivity.this, NavigationDrawerActivity.class));
        }
        else
            Toast.makeText(LoginActivity.this, "Authentication failed.",
                    Toast.LENGTH_SHORT).show();
        // [END sign_in_with_email]
    }

    public void saveValue(String keyPref, String valor) {
        SharedPreferences settings = getSharedPreferences("userdetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putString(keyPref, valor);
        editor.apply();
    }

    public String readValue(String keyPref) {
        SharedPreferences preferences = getSharedPreferences("userdetails",Context.MODE_PRIVATE);
        return  preferences.getString(keyPref, "");
    }

    public void onClick() {
        signIn(false, usernameEditText.getText().toString(), passwordEditText.getText().toString());
    }

    public void signUp() {
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
    }

    @SuppressWarnings("deprecation")
    public void putLanguage(String languageCode) {
        Locale locale = new Locale(languageCode); // where 'hi' is Language code, set this as per your Spinner Item selected
        Locale.setDefault(locale);
        Configuration config = getResources().getConfiguration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }
}
