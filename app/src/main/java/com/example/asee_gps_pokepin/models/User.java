package com.example.asee_gps_pokepin.models;

import com.google.firebase.database.PropertyName;

public class User {

    @PropertyName("name")
    private String name;
    @PropertyName("surname")
    private String surname;
    @PropertyName("email")
    private String email;

    @SuppressWarnings("unused")
    public User() {
        this.name = "";
        this.surname = "";
        this.email = "";
    }

    public User(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

}
