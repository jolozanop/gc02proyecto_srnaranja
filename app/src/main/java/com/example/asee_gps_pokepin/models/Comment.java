package com.example.asee_gps_pokepin.models;

public class Comment {

    private String user, comment;

    @SuppressWarnings("unused")
    public Comment() {

    }

    public Comment (String user, String comment) {
        this.user = user;
        this.comment = comment;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

}
