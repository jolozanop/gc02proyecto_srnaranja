package com.example.asee_gps_pokepin.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Pokemon {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("height")
    @Expose
    private Integer height;

    @SerializedName("weight")
    @Expose
    private Integer weight;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    @SuppressWarnings("unused")
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @NonNull
    @Override
    public String toString() {
        return "Pokemon{" +
                ", id=" + id +
                ", name='" + name + '\'' +
                "height=" + height +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(@NonNull Object p1){
        if (!p1.getClass().isInstance(Pokemon.class)){
            return false;
        }
        else{
            if (p1 instanceof Pokemon){
                return ((Pokemon) p1).getName().equals(this.name) && ((Pokemon) p1).getId().equals(this.id) && ((Pokemon) p1).getHeight().equals(this.height) && ((Pokemon) p1).getWeight().equals(this.weight);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {return 1;}
}
