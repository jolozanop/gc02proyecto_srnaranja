package com.example.asee_gps_pokepin.models;


import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class PokemonMap {

    private String name;
    private double latitude;
    private double longitude;

    @SuppressWarnings("unused")
    public PokemonMap() {

    }

    public PokemonMap(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
