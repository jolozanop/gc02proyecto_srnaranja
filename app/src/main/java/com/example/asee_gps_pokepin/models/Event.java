package com.example.asee_gps_pokepin.models;

import com.google.firebase.database.PropertyName;

import java.util.ArrayList;

public class Event {

    @PropertyName("geoLat")
    private double geoLat;

    @PropertyName("geoLong")
    private double geoLong;

    @PropertyName("userEmail")
    private String userEmail;

    @PropertyName("name")
    private String name;

    @PropertyName("description")
    private String description;

    private ArrayList<Float> ratings = new ArrayList<>();

    @SuppressWarnings("unused")
    public Event (){

    }

    public Event(double geoLat, double geoLong, String userEmail, String name, String description) {
        this.geoLat = geoLat;
        this.geoLong = geoLong;
        this.userEmail = userEmail;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGeoLat() {
        return geoLat;
    }

    public double getGeoLong() {
        return geoLong;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getDescription() {
        return description;
    }

    public void setRatings(ArrayList<Float> ratings) {
        this.ratings = ratings;
    }

    public float calculateAverageRating () {
        float average = 0;
        if (ratings.size() > 0) {
            for (int i = 0; i < ratings.size(); i++) {
                average += ratings.get(i);
            }
            return (float) (Math.round((average / ratings.size())*10.0)/10.0);
        } else {
            return 0;
        }
    }
}
