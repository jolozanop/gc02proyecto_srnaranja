package com.example.asee_gps_pokepin.ui.map.view;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Event;
import com.example.asee_gps_pokepin.ui.map.event.viewmodel.EventViewModel;
import com.example.asee_gps_pokepin.ui.map.pokemonMap.viewmodel.PokemonMapViewModel;
import com.example.asee_gps_pokepin.auth.UserViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.internal.NavigationMenu;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Objects;

import io.github.yavski.fabspeeddial.FabSpeedDial;

import static android.location.LocationManager.GPS_PROVIDER;

public class MapFragment extends Fragment implements OnMapReadyCallback, com.google.android.gms.location.LocationListener {

    private EventViewModel eventViewModel;
    private PokemonMapViewModel pokemonMapViewModel;
    private UserViewModel userViewModel;

    private GoogleMap map;
    private LocationManager locationManager;
    private Location currentLocation;

    private boolean cameraSet = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        eventViewModel =
                ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);

        pokemonMapViewModel =
                ViewModelProviders.of(getActivity()).get(PokemonMapViewModel.class);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);


        View root = inflater.inflate(R.layout.fragment_home, container, false);


        FabSpeedDial fabSpeedDial = root.findViewById(R.id.fabSpeedDial);
        fabSpeedDial.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.action_add_event)
                    Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigate(R.id.add_event_fragment);
                else if (menuItem.getItemId() == R.id.action_add_pokemon)
                    Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigate(R.id.add_pokemon_fragment);
                return true;
            }

            @Override
            public void onMenuClosed() {

            }
        });

        return root;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        locationManager = (LocationManager) Objects.requireNonNull(this.getContext()).getSystemService(Context.LOCATION_SERVICE);
        while (!checkLocationPermission()) ;
        currentLocation = locationManager.getLastKnownLocation(GPS_PROVIDER);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

    }


    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this.getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (!ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(this.getActivity()),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this.getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this.getContext()),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    //Request location updates:
                    String provider = "";
                    locationManager.requestLocationUpdates(provider, 400, 1, (LocationListener) this);
                }

            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        eventViewModel =
                ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);

        pokemonMapViewModel =
                ViewModelProviders.of(getActivity()).get(PokemonMapViewModel.class);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(Objects.requireNonNull(getActivity())));
        ImageLoader imageLoader = ImageLoader.getInstance();

        SharedPreferences preferences = Objects.requireNonNull(getContext()).getSharedPreferences("settings", Context.MODE_PRIVATE);
        boolean showEvents = preferences.getBoolean("no_events", true);

        if (showEvents)
            eventViewModel.getEvents().observe(this, stringEventMap -> {
                map.clear();
                stringEventMap.forEach((k, event) -> {
                boolean showOwned = preferences.getBoolean("just_own", false);
                if (showOwned) {
                    userViewModel.getCurrentUser().observe(this, user -> {
                        if (user != null && user.getEmail().equals(event.getUserEmail())) {
                            Marker m = map.addMarker(new MarkerOptions().title(event.getName()).position(new LatLng(event.getGeoLat(), event.getGeoLong()))
                                    .snippet(event.getDescription()));
                            m.setTag(event);
                        }
                    });
                }else {
                    Marker m = map.addMarker(new MarkerOptions().title(event.getName()).position(new LatLng(event.getGeoLat(), event.getGeoLong()))
                            .snippet(event.getDescription()));
                    m.setTag(event);
                }
            });});

        boolean showPokemonMap = preferences.getBoolean("no_pokemon", true);
        if (showPokemonMap)
            pokemonMapViewModel.getPokemonsMap().observe(this, pokemonsMap -> pokemonsMap.forEach((k, pokemonMap) -> {
                String input = pokemonMap.getName();
                String output = input.substring(0, 1).toUpperCase() + input.substring(1);

                imageLoader.loadImage("https://www.pkparaiso.com/imagenes/xy/sprites/animados/" + pokemonMap.getName() + ".gif", new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        int newWidth = loadedImage.getWidth() * 2;
                        int newHeight = loadedImage.getHeight() * 2;
                        Bitmap bitmap = Bitmap.createScaledBitmap(loadedImage, newWidth, newHeight, true);
                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(pokemonMap.getLatitude(), pokemonMap.getLongitude()))
                                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                .title(output)
                        ).setTag(pokemonMap);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(pokemonMap.getLatitude(), pokemonMap.getLongitude()))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                .title(output)
                        ).setTag(pokemonMap);
                    }
                });

            }));

        map.setOnMarkerClickListener(marker -> {
            Object tag = marker.getTag();

            if (tag instanceof Event) {

                Event event = (Event) tag;

                eventViewModel.setSelected(event);

                Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigate(R.id.show_event_fragment);
            }

            return false;
        });

        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);

        if (!cameraSet) {
            if (currentLocation != null) {
                LatLng current = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15));

                cameraSet = true;
            } else {
                Toast.makeText(getContext(), "Please, turn on location", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
    }

}