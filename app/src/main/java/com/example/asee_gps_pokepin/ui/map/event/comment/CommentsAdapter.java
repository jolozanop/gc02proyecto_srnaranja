package com.example.asee_gps_pokepin.ui.map.event.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Comment;

import java.util.ArrayList;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsHolder> {

    private ArrayList<Comment> listComments = new ArrayList<>();
    private Context c;

    public CommentsAdapter(Context c) {
        this.c = c;
    }

    public void addComment (Comment comment) {
        listComments.add(comment);
        notifyItemInserted(listComments.size());
    }

    @NonNull
    @Override
    public CommentsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.card_view_comments,parent,false);
        return new CommentsHolder(v);
    }

    @Override
    public void onBindViewHolder(CommentsHolder holder, int position) {
        holder.getUser().setText(listComments.get(position).getUser());
        holder.getComment().setText(listComments.get(position).getComment());
    }

    @Override
    public int getItemCount() {
        return listComments.size();
    }

    public void clear(){
        listComments.clear();
        notifyDataSetChanged();
    }
}
