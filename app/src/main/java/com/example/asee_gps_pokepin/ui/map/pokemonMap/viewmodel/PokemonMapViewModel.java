package com.example.asee_gps_pokepin.ui.map.pokemonMap.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.asee_gps_pokepin.data.repository.PokemonMapRepository;
import com.example.asee_gps_pokepin.models.PokemonMap;

import java.util.Map;

public class PokemonMapViewModel extends ViewModel {

    private PokemonMapRepository pokemonMapRepository = PokemonMapRepository.getInstance();

    private MutableLiveData<Map<String,PokemonMap>> pokemonsMap;

    public LiveData<Map<String, PokemonMap>> getPokemonsMap () {
        if(pokemonsMap == null) {
            pokemonsMap = new MutableLiveData<>();
        }
        loadPokemonsMap();
        return pokemonsMap;
    }

    private void loadPokemonsMap() {
        pokemonsMap = (MutableLiveData<Map<String,PokemonMap>>)pokemonMapRepository.getPokemonsMap();
    }

    public void addPokemonToMap(PokemonMap pokemonMap){
        pokemonMapRepository.insertPokemonMap(pokemonMap);
    }

}
