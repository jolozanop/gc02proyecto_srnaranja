package com.example.asee_gps_pokepin.ui.pokedex.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdvancedSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdvancedSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdvancedSearchFragment extends Fragment {

    private EditText nameEditText;
    private EditText idEditText;

    private EditText minWeightEditText;
    private EditText maxWeightEditText;

    private EditText minHeightEditText;
    private EditText maxHeightEditText;

    private PokedexViewModel pokedexViewModel;


    private advSearch f;

    private AdvancedSearchFragment(advSearch f) {
        this.f=f;
        // Required empty public constructor
    }

    @SuppressWarnings("WeakerAccess")
    public static AdvancedSearchFragment newInstance(PokedexFragment f) {
        return new AdvancedSearchFragment(f);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pokedexViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokedexViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_advanced_search, container, false);


        // Inflate the layout for this fragment
        nameEditText = root.findViewById(R.id.name_editText);
        idEditText = root.findViewById(R.id.id_editText);

        minWeightEditText = root.findViewById(R.id.minWeight_editText);
        maxWeightEditText = root.findViewById(R.id.maxWeight_editText);

        minHeightEditText = root.findViewById(R.id.minHeight_editText);
        maxHeightEditText = root.findViewById(R.id.maxHeight_editText);


        FloatingActionButton actionButton = root.findViewById(R.id.floatingActionButton);
        actionButton.setOnClickListener(v -> advSearchClick());


        return root;
    }

    private void advSearchClick(){
        getBundle().observe(this, pokemons -> f.onClickAdv(pokemons));

    }

    public interface advSearch{
        void onClickAdv(List<Pokemon> list);
    }

    public LiveData<List<Pokemon>> getBundle(){
        String name;
        if (!nameEditText.getText().toString().isEmpty())
            name = nameEditText.getText().toString().toLowerCase();
        else name="";

        Integer id = null;
        if (!idEditText.getText().toString().isEmpty())
            id = Integer.parseInt(idEditText.getText().toString().toLowerCase());


        int minW = getResources().getInteger(R.integer.minWeight);
        if (!minWeightEditText.getText().toString().isEmpty())
            minW = Integer.parseInt(minWeightEditText.getText().toString());

        int maxW = getResources().getInteger(R.integer.maxWeight);
        if (!maxWeightEditText.getText().toString().isEmpty())
            maxW = Integer.parseInt(maxWeightEditText.getText().toString());

        int minH = getResources().getInteger(R.integer.minHeight);
        if (!minHeightEditText.getText().toString().isEmpty())
            minH = Integer.parseInt(minHeightEditText.getText().toString());

        int maxH = getResources().getInteger(R.integer.maxHeight);
        if (!maxHeightEditText.getText().toString().isEmpty())
            maxH = Integer.parseInt(maxHeightEditText.getText().toString());

        return pokedexViewModel.advSearch(id, name, minH, maxH, minW, maxW);
    }



    @Override
    public void onDetach() {
        super.onDetach();
    }
    interface OnFragmentInteractionListener {
    }
}
