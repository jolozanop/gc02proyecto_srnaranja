package com.example.asee_gps_pokepin.ui.pokedex.view;

import android.annotation.SuppressLint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.ui.favs.viewmodel.PokemonFavViewModel;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;

import java.io.IOException;
import java.util.Objects;

public class PokemonFragment extends Fragment {

    private View root;

    private boolean shiny;

    private PokemonFavViewModel pokemonFavViewModel;
    private Button fav_button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PokedexViewModel pokedexViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokedexViewModel.class);
        pokemonFavViewModel = ViewModelProviders.of(getActivity()).get(PokemonFavViewModel.class);

        root = inflater.inflate(R.layout.fragment_pokemon, container, false);
        ImageView image_pokemon = root.findViewById(R.id.image_view_pokemon_info);
        root.findViewById(R.id.button_fav);

        Button activate_shiny = root.findViewById(R.id.button_activate_shiny);
        TextView text_pokemon_name = root.findViewById(R.id.text_view_pokemon_info_name);
        TextView text_pokemon_height_value = root.findViewById(R.id.text_view_pokemon_info_height_value);
        TextView text_pokemon_weight_value = root.findViewById(R.id.text_view_pokemon_info_weight_value);
        shiny = false;

        fav_button = root.findViewById(R.id.button_fav);


        pokedexViewModel.getSelected().observe(this, pokemon -> {

            text_pokemon_name.setText(pokemon.getName());
            String name = pokemon.getName().substring(0, 1).toUpperCase() + pokemon.getName().substring(1);
            text_pokemon_name.setText(name);
            String height = (pokemon.getHeight() / 10.0) + " m";
            text_pokemon_height_value.setText(height);
            String weight = (pokemon.getWeight() / 10.0) + " kg";
            text_pokemon_weight_value.setText(weight);




            fav_button.setOnClickListener(view -> {
                pokemonFavViewModel.switchFavourite(pokemon.getId());
                pokemonFavViewModel.isPokemonFav(pokemon.getId()).observe(getActivity(), isFav -> {
                    if (!isFav)
                        fav_button.setBackgroundResource(android.R.drawable.star_big_on);
                    else
                        fav_button.setBackgroundResource(android.R.drawable.star_big_off);
                });
            });


            pokemonFavViewModel.isPokemonFav(pokemon.getId()).observe(Objects.requireNonNull(getActivity()), isFav -> {
                if (isFav)
                    fav_button.setBackgroundResource(android.R.drawable.star_big_on);
                else
                    fav_button.setBackgroundResource(android.R.drawable.star_big_off);
            });

            Glide.with(root)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemon.getId() + ".png")
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image_pokemon);
            activate_shiny.setOnClickListener(view -> changeImage(pokemon));

            @SuppressLint("DefaultLocale") String id = String.format("%03d", pokemon.getId());

            String url = "https://github.com/ZeChrales/PogoAssets/blob/master/pokemon_cries/pv" + id + ".wav?raw=true"; // your URL here
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(url);
                mediaPlayer.prepareAsync(); // might take long! (for buffering, etc)
            } catch (IOException e) {
                e.printStackTrace();
            }

            mediaPlayer.setOnPreparedListener(MediaPlayer::start);


        });


        return root;
    }


    private void changeImage(Pokemon pokemon) {
        ImageView image_pokemon = root.findViewById(R.id.image_view_pokemon_info);
        Button b = root.findViewById(R.id.button_activate_shiny);
        if (!shiny){
            b.setText(R.string.test_pokemon_info_shiny_deactivate);
            Glide.with(root)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" + pokemon.getId() + ".png")
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image_pokemon);
            shiny = true;
        }else{
            b.setText(R.string.test_pokemon_info_shiny_activate);
            Glide.with(root)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemon.getId() + ".png")
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image_pokemon);
            shiny = false;
        }
    }


}
