package com.example.asee_gps_pokepin.ui.map.event.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.asee_gps_pokepin.data.repository.EventRepository;
import com.example.asee_gps_pokepin.models.Comment;
import com.example.asee_gps_pokepin.models.Event;

import java.util.List;
import java.util.Map;

public class EventViewModel extends ViewModel {
    private MutableLiveData<Map<String, Event>> events;

    private final MutableLiveData<Event> selected = new MutableLiveData<>();

    private final MutableLiveData<String> selectedKey = new MutableLiveData<>();

    private EventRepository eventRepository = EventRepository.getInstance();

    public LiveData<Map<String,Event>> getEvents() {
        if (events == null) {
            events = new MutableLiveData<>();
        }
        loadEvents();
        return events;
    }

    private void loadEvents() {
        events = (MutableLiveData<Map<String, Event>>) eventRepository.getEvents();
    }

    public void setSelected(Event event) {
        selected.setValue(event);
    }

    public LiveData<Event> getSelected() {
        return selected;
    }

    public void setSelectedKey(String key) {
        selectedKey.setValue(key);
    }

    public LiveData<String> getSelectedKey() {
        return selectedKey;
    }

    public void updateEvent(String key, double geoLat, double geoLong, String name, String description) {
        eventRepository.updateEvent(key, geoLat, geoLong, name, description);
    }

    public void insertEvent(double geoLat, double geoLong, String name, String description) {
        eventRepository.insertEvent(geoLat, geoLong, name, description);
    }

    public void deleteEvent (String key) {
        eventRepository.deleteEvent(key);
    }

    public void rate (float rating) {
        eventRepository.rate(rating, getSelectedKey().getValue());
    }

    public void sendComment (String message) {
        eventRepository.sendComment(message, getSelectedKey().getValue());
    }

    public LiveData<List<Comment>> getComments() {
        return eventRepository.getCommentsEvent(selectedKey.getValue());
    }

}
