package com.example.asee_gps_pokepin.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.asee_gps_pokepin.LoginActivity;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.auth.UserViewModel;
import com.google.android.material.navigation.NavigationView;

public class NavigationDrawerActivity extends AppCompatActivity{

    private AppBarConfiguration mAppBarConfiguration;

    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        final TextView navUserName = headerView.findViewById(R.id.userName);
        final TextView navUserEmail = headerView.findViewById(R.id.userEmail);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        userViewModel.getCurrentUser().observe(this, user -> {
            if(user != null) {
                navUserName.setText(String.format("%s %s",user.getName(), user.getSurname()));
                navUserEmail.setText(user.getEmail());
            }
        });

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_mapa, R.id.nav_pokedex, R.id.nav_favs, R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    public void logout () {
        userViewModel.signOut();

        SharedPreferences settings = getSharedPreferences("userdetails",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();

        editor.remove("user");
        editor.remove("password");
        editor.apply();

        Intent intent = new Intent(NavigationDrawerActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finishAffinity();
        startActivity(new Intent(NavigationDrawerActivity.this, LoginActivity.class));
    }



    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.nav_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }


}
