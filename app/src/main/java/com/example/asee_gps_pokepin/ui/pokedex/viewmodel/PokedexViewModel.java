package com.example.asee_gps_pokepin.ui.pokedex.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.data.repository.PokemonRepository;
import com.example.asee_gps_pokepin.models.Pokemon;

import java.util.List;

public class PokedexViewModel extends AndroidViewModel{

    private PokemonRepository mRepository;

    private LiveData<List<Pokemon>> mAllPokemon;

    private MutableLiveData<Pokemon> selected = new MutableLiveData<>();

    private MutableLiveData<List<Pokemon>> queryPokemonData = new MutableLiveData<>();


    public PokedexViewModel (Application application) {
        super(application);
        mRepository = PokemonRepository.getInstance(application);
        mAllPokemon = mRepository.getAllPokemon();
    }

    public LiveData<List<Pokemon>> getAllPokemon() {
        return mAllPokemon;
    }

    public LiveData<List<Pokemon>> searchByName(String s) {
        return mRepository.searchByName(s);
    }

    public void select(Pokemon pokemon) {
        selected.setValue(pokemon);
    }

    public LiveData<Pokemon> getSelected() {
        return selected;
    }

    public LiveData<Pokemon> searchSingleByName(String name){
        return mRepository.searchSingleByName(name);
    }


    public LiveData<List<Pokemon>> advSearch(Integer id, String name, Integer minH, Integer maxH, Integer minW, Integer maxW){
        mRepository.getAllPokemon().observeForever(pokemons ->{
            if (id == null){
                pokemons.removeIf(pokemon -> (!pokemon.getName().contains(name) ||
                        ( pokemon.getHeight() < minH) || (pokemon.getHeight() > maxH) ||
                        (pokemon.getWeight() < minW) || (pokemon.getWeight() > maxW)));
            }
            else{
                pokemons.removeIf(pokemon -> (!pokemon.getName().contains(name) || !pokemon.getId().equals(id) ||
                        ( pokemon.getHeight() < minH) || (pokemon.getHeight() > maxH) ||
                        (pokemon.getWeight() < minW) || (pokemon.getWeight() > maxW)));
            }

            queryPokemonData.postValue(pokemons);
        } );


        return queryPokemonData;
    }

}