package com.example.asee_gps_pokepin.ui.pokedex.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.asee_gps_pokepin.ui.pokedex.ListPokemonAdapter;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

public class PokedexFragment extends Fragment implements AdvancedSearchFragment.advSearch {

    private ListPokemonAdapter listPokemonAdapter;

    private PokedexViewModel mPokedexViewModel;

    private FrameLayout fragmentContainer;
    private Boolean hiddenSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_pokedex, container, false);

        mPokedexViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokedexViewModel.class);

        listPokemonAdapter = new ListPokemonAdapter(this.getContext(), mPokedexViewModel);

        mPokedexViewModel.getAllPokemon().observe(this, allPokemon -> listPokemonAdapter.addListaPokemon(allPokemon));


        SearchView searchView = root.findViewById(R.id.search_View);
        fragmentContainer = root.findViewById(R.id.fragment_container);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!s.isEmpty()){
                    String finalS = s.toLowerCase();
                    mPokedexViewModel.searchByName(finalS).observe(Objects.requireNonNull(getActivity()), pokemonNames -> listPokemonAdapter.addListaPokemon(pokemonNames));
                }
                else {
                    mPokedexViewModel.getAllPokemon().observe(Objects.requireNonNull(getActivity()), pokemons -> listPokemonAdapter.addListaPokemon(pokemons));

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.isEmpty())
                    mPokedexViewModel.getAllPokemon().observe(Objects.requireNonNull(getActivity()), pokemons -> listPokemonAdapter.addListaPokemon(pokemons));

                return false;
            }
        });

        FloatingActionButton advSearchButton = root.findViewById(R.id.advSearchButton);

        advSearchButton.setOnClickListener(v -> onClickBusquedaAvanzada());

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(listPokemonAdapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(null, 3);
        recyclerView.setLayoutManager(layoutManager);

        hiddenSearch=true;

        return root;
    }

    private void onClickBusquedaAvanzada()  {
        if(hiddenSearch){
            Fragment advSearchFragment = AdvancedSearchFragment.newInstance(this);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = Objects.requireNonNull(fragmentManager).beginTransaction();
            transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, R.anim.pop_enter, R.anim.pop_exit);
            transaction.addToBackStack(null);
            transaction.add(R.id.fragment_container, advSearchFragment, "AdvancedSearchFragment").commit();
            hiddenSearch=false;
        }
        else{
            fragmentContainer.removeAllViews();
            hiddenSearch=true;
        }
    }


    @Override
    public void onClickAdv(List<Pokemon> list) {
        listPokemonAdapter.addListaPokemon(list);
        fragmentContainer.removeAllViews();
        hiddenSearch=true;

    }
}
