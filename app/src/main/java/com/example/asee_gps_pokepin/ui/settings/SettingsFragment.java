package com.example.asee_gps_pokepin.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import com.example.asee_gps_pokepin.R;

import java.util.Locale;
import java.util.Objects;

public class SettingsFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        Switch no_events = root.findViewById(R.id.select_no_events);
        Switch no_pokemon = root.findViewById(R.id.select_no_pokemon);
        Switch just_own = root.findViewById(R.id.select_only_own);
        Spinner select_language = root.findViewById(R.id.select_language);

        no_events.setChecked(Objects.requireNonNull(getContext()).getSharedPreferences("settings",Context.MODE_PRIVATE).getBoolean("no_events", true));
        no_pokemon.setChecked(getContext().getSharedPreferences("settings",Context.MODE_PRIVATE).getBoolean("no_pokemon", true));
        just_own.setChecked(getContext().getSharedPreferences("settings",Context.MODE_PRIVATE).getBoolean("just_own", false));


        no_events.setOnCheckedChangeListener((buttonView, isChecked) -> setValue("no_events", isChecked));

        no_pokemon.setOnCheckedChangeListener((buttonView, isChecked) -> setValue("no_pokemon", isChecked));

        just_own.setOnCheckedChangeListener((buttonView, isChecked) -> setValue("just_own", isChecked));

        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(getContext(),
                        R.array.languages_array,
                        android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        select_language.setAdapter(adapter);

        select_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String language = parent.getItemAtPosition(position).toString();
                changeLanguage(language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }

    private void setValue(String keyPref, boolean valor) {
        SharedPreferences settings = Objects.requireNonNull(getContext()).getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putBoolean(keyPref, valor);
        editor.apply();
    }

    private void changeLanguage(String language) {
        String languageCode = null;

        switch (language) {
            case "English":
            case "Inglés":
                languageCode = "en";
                break;
            case "Spanish":
            case "Español":
                languageCode = "es";
                break;
        }

        String currentLanguage = readValue("language");
        if (languageCode != null && !languageCode.equals(currentLanguage) && !currentLanguage.equals("")) {
            Locale locale = new Locale(languageCode); // where 'hi' is Language code, set this as per your Spinner Item selected
            Locale.setDefault(locale);
            Configuration config = Objects.requireNonNull(getContext()).getResources().getConfiguration();
            config.setLocale(locale);
            getContext().getResources().updateConfiguration(config, getContext().getResources().getDisplayMetrics());
            Objects.requireNonNull(getActivity()).finish();
            getActivity().startActivity(getActivity().getIntent());
        }

        SharedPreferences settings = Objects.requireNonNull(getContext()).getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putString("language", languageCode);
        editor.apply();
    }

    private String readValue(@SuppressWarnings("SameParameterValue") String keyPref) {
        SharedPreferences preferences = Objects.requireNonNull(getContext()).getSharedPreferences("settings",Context.MODE_PRIVATE);
        return  preferences.getString(keyPref, "");
    }

}
