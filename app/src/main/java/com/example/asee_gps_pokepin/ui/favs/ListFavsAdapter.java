package com.example.asee_gps_pokepin.ui.favs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListFavsAdapter extends RecyclerView.Adapter<ListFavsAdapter.ViewHolder>{
    private List<Pokemon> pokemonsFavList;
    private Context context;
    private PokedexViewModel pokedexViewModel;

    public ListFavsAdapter(Context context) {
        this.context = context;
        pokemonsFavList = new ArrayList<>();
        pokedexViewModel = ViewModelProviders.of((FragmentActivity) context).get(PokedexViewModel.class);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fav, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Pokemon p = pokemonsFavList.get(position);
        holder.nombreTextView.setText(p.getName().substring(0,1).toUpperCase() + p.getName().substring(1));

        Glide.with(context)
                .load("http://www.pokestadium.com/sprites/xy/"+ p.getName()+ ".gif")
                .into(holder.fotoImageView);

        holder.fotoImageView.setOnClickListener(view -> {
            pokedexViewModel.select(p);
            Navigation.findNavController((Activity)context, R.id.nav_host_fragment).navigate(R.id.pokemon_info_fragment);
        });
    }

    @Override
    public int getItemCount() {
        return this.pokemonsFavList.size();
    }

    public void addListaPokemon(List<Pokemon> listaPokemon) {
        pokemonsFavList.clear();
        pokemonsFavList.addAll(listaPokemon);
        notifyDataSetChanged();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView fotoImageView;
        private TextView nombreTextView;

        ViewHolder(View itemView) {
            super(itemView);

            fotoImageView = itemView.findViewById(R.id.fav_image);
            nombreTextView = itemView.findViewById(R.id.fav_name);
        }
    }
}
