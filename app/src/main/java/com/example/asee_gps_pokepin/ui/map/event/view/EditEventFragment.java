package com.example.asee_gps_pokepin.ui.map.event.view;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.ui.map.event.viewmodel.EventViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EditEventFragment extends Fragment {

    private EditText event_name_edit, event_description_edit, event_address_edit;


    private EventViewModel eventViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        eventViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);
        View root = inflater.inflate(R.layout.fragment_edit_event, container, false);

        event_name_edit = root.findViewById(R.id.event_name_edit);
        event_description_edit = root.findViewById(R.id.event_description_edit);
        event_address_edit = root.findViewById(R.id.event_address_edit);

        Button modifyEvent = root.findViewById(R.id.modify_event);
        modifyEvent.setOnClickListener(v -> editEvent());

        eventViewModel.getSelected().observe(this, event -> {
            Geocoder geo = new Geocoder(getContext());
            List<Address> addresses;
            try {
                addresses = geo.getFromLocation(event.getGeoLat(), event.getGeoLong(), 1);
                event_address_edit.setText(addresses.get(0).getAddressLine(0));
                event_name_edit.setText(event.getName());
                event_description_edit.setText(event.getDescription());

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return root;
    }


    private void editEvent() {
        String eName, eDescription, eAddress;
        eName = event_name_edit.getText().toString();
        eDescription = event_description_edit.getText().toString();
        eAddress = event_address_edit.getText().toString();
        if (eName.isEmpty()) {
            Toast.makeText(EditEventFragment.this.getContext(), "Please enter a name.", Toast.LENGTH_SHORT).show();
        } else {

            if (!eAddress.isEmpty()) {
                Geocoder geo = new Geocoder(EditEventFragment.this.getContext());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geo.getFromLocationName(eAddress, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses.size() > 0) {
                    double lat = addresses.get(0).getLatitude();
                    double lon = addresses.get(0).getLongitude();
                    eventViewModel.getSelectedKey().observe(this, s -> {
                        eventViewModel.updateEvent(s, lat, lon, eName, eDescription);
                        Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigateUp();
                    });

                }
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
