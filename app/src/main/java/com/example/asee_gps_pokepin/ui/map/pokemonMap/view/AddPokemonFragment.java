package com.example.asee_gps_pokepin.ui.map.pokemonMap.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.PokemonMap;
import com.example.asee_gps_pokepin.ui.map.pokemonMap.viewmodel.PokemonMapViewModel;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class AddPokemonFragment extends Fragment {

    private PokemonMapViewModel pokemonMapViewModel;

    private PokedexViewModel pokedexViewModel;

    private LocationManager locationManager;
    private Location currentLocation;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        pokemonMapViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokemonMapViewModel.class);
        View root = inflater.inflate(R.layout.fragment_add_pokemon, container, false);

        pokedexViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokedexViewModel.class);

        TextInputEditText inputText = root.findViewById(R.id.pokemon_input);
        Button buttonAddPokemon = root.findViewById(R.id.button_add_pokemon);

        buttonAddPokemon.setOnClickListener((View v) -> pokedexViewModel.searchSingleByName(Objects.requireNonNull(inputText.getText()).toString().toLowerCase()).observe(this, pokemon -> {
            if (pokemon != null && pokemon.getName().toLowerCase().equals(inputText.getText().toString().toLowerCase())) {
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                checkLocationPermission();
                currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                PokemonMap pokemonMap = new PokemonMap(pokemon.getName(), currentLocation.getLatitude(), currentLocation.getLongitude());
                pokemonMapViewModel.addPokemonToMap(pokemonMap);
                Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigateUp();
            }
            else {
                inputText.setError("This pokemon doesn't exist");
            }
        }));

        return root;
    }

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (!ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }
}
