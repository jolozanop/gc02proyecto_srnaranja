package com.example.asee_gps_pokepin.ui.map.event.comment.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.ui.map.event.comment.CommentsAdapter;
import com.example.asee_gps_pokepin.ui.map.event.viewmodel.EventViewModel;

import java.util.Objects;

public class EventCommentsFragment extends Fragment {

    private EventViewModel eventViewModel;

    private RecyclerView comments;
    private CommentsAdapter adapter;

    private EditText write_comment;
    private TextView no_comments;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_event_comments, container, false);
        eventViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);

        comments = root.findViewById(R.id.comments);
        write_comment = root.findViewById(R.id.write_comment);
        no_comments = root.findViewById(R.id.no_comments);
        Button send_comment = root.findViewById(R.id.send_comment);

        adapter = new CommentsAdapter(getContext());
        LinearLayoutManager l = new LinearLayoutManager(getContext());
        comments.setLayoutManager(l);
        comments.setAdapter(adapter);

        send_comment.setOnClickListener(v -> sendComment());

        eventViewModel.getComments().observe(this, commentsList -> { adapter.clear(); commentsList.forEach(comment -> {
            no_comments.setVisibility(View.GONE);
            adapter.addComment(comment);
        });});

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollBar();
            }
        });

    }

    private void sendComment() {
        if (!write_comment.getText().toString().equals("")) {
            adapter.clear();
            eventViewModel.sendComment(write_comment.getText().toString());
            write_comment.setText("");
        }
    }

    private void setScrollBar() {
        comments.scrollToPosition(adapter.getItemCount()-1);
    }


}
