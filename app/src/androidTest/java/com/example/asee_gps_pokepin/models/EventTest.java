package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EventTest {

    private Event eventUnderTest;

    @Before
    public void setUp() {
        eventUnderTest = new Event(0.0, 0.0, "userEmail", "name", "description");
    }

    @Test
    public void getName() {
        assertEquals(eventUnderTest.getName(), "name");
    }

    @Test
    public void setName() {
        eventUnderTest.setName("testName");
        assertEquals(eventUnderTest.getName(), "testName");
    }

    @Test
    public void getGeoLat() {
        assertEquals(0.0, eventUnderTest.getGeoLat(), 0.0);
    }

    @Test
    public void getGeoLong() {
        assertEquals(0.0, eventUnderTest.getGeoLong(), 0.0);
    }

    @Test
    public void getUserEmail() {
        assertEquals(eventUnderTest.getUserEmail(), "userEmail");
    }

    @Test
    public void getDescription() {
        assertEquals(eventUnderTest.getDescription(), "description");
    }

    @Test
    public void testCalculateAverageRating_And_SetRatings() {
        // Setup
        ArrayList<Float> ratings = new ArrayList<>();
        ratings.add(0.0f);
        ratings.add(5.0f);
        eventUnderTest.setRatings(ratings);

        // Run the test
        final float result = eventUnderTest.calculateAverageRating();

        // Verify the results
        assertEquals(2.5f, result, 0.0001);
    }
}
