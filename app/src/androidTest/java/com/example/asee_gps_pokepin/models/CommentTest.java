package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class CommentTest {

    private Comment commentUnderTest;

    @Before
    public void setUp() {
        commentUnderTest = new Comment("user", "comment");
    }

    @Test
    public void testGetUser() {
        assertEquals(commentUnderTest.getUser(), "user");
    }

    @Test
    public void testSetUser() {
        commentUnderTest.setUser("test_user");
        assertEquals(commentUnderTest.getUser(), "test_user");
    }

    @Test
    public void testGetComment() {
        assertEquals(commentUnderTest.getComment(), "comment");
    }
}
