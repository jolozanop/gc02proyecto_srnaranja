package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonTest {

    private Pokemon pokemonUnderTest;

    @Before
    public void setUp() {

        pokemonUnderTest = new Pokemon();
        pokemonUnderTest.setHeight(10);
        pokemonUnderTest.setId(2);
        pokemonUnderTest.setName("mew");
        pokemonUnderTest.setWeight(50);
    }

    @Test
    public void testGetHeight() {
        // Setup

        // Run the test
        final int result = pokemonUnderTest.getHeight();
        // Verify the results
        assertEquals(10, result);
    }

    @Test
    public void testGetWeight() {
        // Setup

        // Run the test
        final int result = pokemonUnderTest.getWeight();
        // Verify the results
        assertEquals(50, result);
    }

    @Test
    public void testGetName() {
        // Setup

        // Run the test
        final String result = pokemonUnderTest.getName();
        // Verify the results
        assertEquals("mew", result);
    }

    @Test
    public void testGetId() {
        // Setup

        // Run the test
        final int result = pokemonUnderTest.getId();
        // Verify the results
        assertEquals(2, result);
    }

    @Test
    public void testSetId() {
        // Setup

        // Run the test
        pokemonUnderTest.setId(3);
        final int result = pokemonUnderTest.getId();
        // Verify the results
        assertEquals(3, result);
    }
    @Test
    public void testSetHeight() {
        // Setup

        // Run the test
        pokemonUnderTest.setHeight(3);
        final int result = pokemonUnderTest.getHeight();
        // Verify the results
        assertEquals(3, result);
    }
    @Test
    public void testSetWeight() {
        // Setup

        // Run the test
        pokemonUnderTest.setWeight(3);
        final int result = pokemonUnderTest.getWeight();
        // Verify the results
        assertEquals(3, result);
    }
    @Test
    public void testSetName() {
        // Setup

        // Run the test
        pokemonUnderTest.setName("mew");
        final String result = pokemonUnderTest.getName();
        // Verify the results
        assertEquals("mew", result);
    }
}
