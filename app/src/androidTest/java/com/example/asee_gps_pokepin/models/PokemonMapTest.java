package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonMapTest {

    private PokemonMap pokemonMapUnderTest;

    @Before
    public void setUp() {
        pokemonMapUnderTest = new PokemonMap("name", 0.0, 0.0);
    }

    @Test
    public void testGetLatitude() {
        assertEquals(0.0, pokemonMapUnderTest.getLatitude(), 0.0);
    }

    @Test
    public void testGetLongitude() {
        assertEquals(0.0, pokemonMapUnderTest.getLongitude(), 0.0);
    }

    @Test
    public void testGetName() {
        assertEquals(pokemonMapUnderTest.getName(), "name");
    }

    @Test
    public void testSetName() {
        pokemonMapUnderTest.setName("testName");
        assertEquals(pokemonMapUnderTest.getName(), "testName");
    }
}
