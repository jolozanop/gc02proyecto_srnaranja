package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class UserTest {

    private User userUnderTest;

    @Before
    public void setUp() {
        userUnderTest = new User("name", "surname", "email");
    }

    @Test
    public void testGetName() {
        assertEquals(userUnderTest.getName(),("name"));
    }

    @Test
    public void testSetName() {
        userUnderTest.setName("Antonio");
        assertEquals(userUnderTest.getName(),"Antonio");
    }

    @Test
    public void testGetSurname() {
        assertEquals(userUnderTest.getSurname(),"surname");
    }

    @Test
    public void testGetEmail() {
        assertEquals("email", userUnderTest.getEmail());
    }
}
