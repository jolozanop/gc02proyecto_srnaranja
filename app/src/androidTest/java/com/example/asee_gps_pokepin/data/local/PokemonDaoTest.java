package com.example.asee_gps_pokepin.data.local;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.LiveDataTestUtils;
import com.example.asee_gps_pokepin.models.Pokemon;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonDaoTest {

    private AppDatabase appDatabaseUnderTest;
    private PokemonDao pokemonDao;
    private Pokemon p;
    private LiveData<Pokemon> mut = new MutableLiveData<>();

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {

        Context context = getInstrumentation().getTargetContext();
        appDatabaseUnderTest = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).allowMainThreadQueries().build();

        pokemonDao = appDatabaseUnderTest.getDao();

        p = new Pokemon();
        p.setName("Mew");
        p.setWeight(10);
        p.setHeight(50);
        p.setId(67);


    }

    @After
    public void closeDB(){
        appDatabaseUnderTest.close();
    }

    @Test
    public void testRead_And_Insert_And_GetByNamePokemon() throws InterruptedException {
        pokemonDao.insert(p);

        LiveData<Pokemon> livePokemon = pokemonDao.getByName("Mew");
        Pokemon pok = LiveDataTestUtils.getValue(livePokemon);

        assertEquals(pok.getName(), p.getName());
    }
    @Test
    public void testGetAllPokemon() throws InterruptedException {
        Pokemon p1 = crearPokemon("Uno", 1, 1, 1);
        Pokemon p2 = crearPokemon("Dos", 2, 2, 2);
        Pokemon p3 = crearPokemon("Tres", 3, 3, 3);
        pokemonDao.insert(p1);
        pokemonDao.insert(p2);
        pokemonDao.insert(p3);

        LiveData<List<Pokemon>> livePokemon = pokemonDao.getAll();
        List<Pokemon> pokList = LiveDataTestUtils.getValue(livePokemon);

        boolean found = false;

        for (Pokemon p: pokList){
            if (p.getName().equals(p1.getName())) {
                found = true;
                break;
            }

        }

        assertTrue(found);
    }

    /**

    @Test
    public void testGetAllByName() throws InterruptedException {
        Pokemon p1 = crearPokemon("Uno", 1, 1, 1);
        Pokemon p2 = crearPokemon("Dos", 2, 2, 2);
        Pokemon p3 = crearPokemon("Tres", 3, 3, 3);
        pokemonDao.insert(p1);
        pokemonDao.insert(p2);
        pokemonDao.insert(p3);

        LiveData<List<Pokemon>> livePokemon = pokemonDao.getAllByName("s");
        List<Pokemon> pokList = LiveDataTestUtils.getValue(livePokemon);

        int count = 0;
        for (Pokemon p : pokList)
            if (p.getName().equals(p2.getName()) ||  p.getName().equals(p3.getName()))
                count++;

        assertEquals(2, count);
    }

    @Test
    public void testGetPokemonById() throws InterruptedException {
        Pokemon p1 = crearPokemon("Uno", 1, 1, 1);
        Pokemon p2 = crearPokemon("Dos", 2, 2, 2);
        Pokemon p3 = crearPokemon("Tres", 3, 3, 3);
        pokemonDao.insert(p1);
        pokemonDao.insert(p2);
        pokemonDao.insert(p3);

        LiveData<Pokemon> livePokemon = pokemonDao.getPokemon(2);
        Pokemon pok = LiveDataTestUtils.getValue(livePokemon);

        assertTrue(pok.getName().equals(p2.getName()));

    }
    **/

    public Pokemon crearPokemon(String name, int id, int weight, int height){
        Pokemon p = new Pokemon ();
        p.setName(name);
        p.setId(id);
        p.setHeight(height);
        p.setWeight(weight);
        return p;
    }
}
