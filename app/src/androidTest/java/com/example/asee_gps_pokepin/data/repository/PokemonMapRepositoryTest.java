package com.example.asee_gps_pokepin.data.repository;

import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.models.PokemonMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonMapRepositoryTest {

    private PokemonMapRepository pokemonMapRepositoryUnderTest;

    @Before
    public void setUp() {
        pokemonMapRepositoryUnderTest = Mockito.mock(PokemonMapRepository.class);
    }

    @Test
    public void testGetPokemonsMap_And_InsertPokemonMap() {
        // Setup
        String name = "testPokemonMap";
        PokemonMap pokemonMap = new PokemonMap(name, 0.0, 0.0);
        Map <String,PokemonMap> mapPokemonsMap = new HashMap<>();
        mapPokemonsMap.put(name, pokemonMap);

        pokemonMapRepositoryUnderTest.insertPokemonMap(pokemonMap);

        // Run the test
        when(pokemonMapRepositoryUnderTest.getPokemonsMap()).thenReturn(new MutableLiveData<>(mapPokemonsMap));

        // Verify the results
        assertEquals(pokemonMapRepositoryUnderTest.getPokemonsMap().getValue(), mapPokemonsMap);
    }
}
