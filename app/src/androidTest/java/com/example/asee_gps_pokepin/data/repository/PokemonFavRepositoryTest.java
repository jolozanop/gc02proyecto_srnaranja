package com.example.asee_gps_pokepin.data.repository;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.models.Pokemon;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonFavRepositoryTest {

    @Mock
    private PokemonFavRepository pokemonFavRepositoryUnderTest;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();


    @Before
    public void setUp() {
        pokemonFavRepositoryUnderTest = Mockito.mock(PokemonFavRepository.class);
    }

    @Test
    public void testGetPokemonFav() {
        // Setup
        MutableLiveData<List<Pokemon>> pok = new MutableLiveData<>();
        List<Pokemon> list = new ArrayList<>();
        list.add(crearPokemon("Uno", 1, 1, 1));
        list.add(crearPokemon("Dos", 2, 2, 2));
        list.add(crearPokemon("Tres", 3, 3, 3));
        list.add(crearPokemon("Cuatro", 4, 4, 4));
        list.add(crearPokemon("Cinco", 5, 5, 5));
        pok.postValue(list);

        // Run the test
        when(pokemonFavRepositoryUnderTest.getPokemonFav()).thenReturn(pok);

        // Verify the results
        assertEquals(pokemonFavRepositoryUnderTest.getPokemonFav(), pok);
    }

    @Test
    public void testIsFavourite() {
        // Setup
        MutableLiveData<Pokemon> pok = new MutableLiveData<>();
        Pokemon pokemon = crearPokemon("Dos", 2, 2, 2);
        pok.postValue(pokemon);
        MutableLiveData<Boolean> bool = new MutableLiveData<>();
        bool.postValue(true);

        when(pokemonFavRepositoryUnderTest.isFavourite(2)).thenReturn(bool);
        // Verify the results
        assertEquals(bool, pokemonFavRepositoryUnderTest.isFavourite(2));
    }

    public Pokemon crearPokemon(String name, int id, int weight, int height){
        Pokemon p = new Pokemon ();
        p.setName(name);
        p.setId(id);
        p.setHeight(height);
        p.setWeight(weight);
        return p;
    }
}
