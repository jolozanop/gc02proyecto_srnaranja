package com.example.asee_gps_pokepin.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class UserRepositoryTest {

    @Mock
    private UserRepository userRepositoryUnderTest;

    @Before
    public void setUp(){
        userRepositoryUnderTest = Mockito.mock(UserRepository.class);
    }

    @Test
    public void testSignInWithEmailAndPassword() {
        // Setup
        String email = "email@test.com";
        String password = "test123";

        // Run the test
        when(userRepositoryUnderTest.signInWithEmailAndPassword(email, password)).thenReturn(true);

        // Verify the results
        assertTrue(userRepositoryUnderTest.signInWithEmailAndPassword(email, password));
    }

    @Test
    public void testSignInWithEmailAndPasswordFailed() {
        // Setup
        String email = "email@test.com";
        String password = "password";

        // Run the test
        when(userRepositoryUnderTest.signInWithEmailAndPassword(email, password)).thenReturn(false);

        // Verify the results
        assertFalse(userRepositoryUnderTest.signInWithEmailAndPassword(email, password));

    }

    @Test
    public void testCreateUserWithEmailAndPassword() {
        // Setup
        String email = "email@test.com";
        String name = "name";
        String surname = "surname";
        final User u = new User(name, surname, email);

        // Run the test
        when(userRepositoryUnderTest.createUserWithEmailAndPassword(email, "test123", u)).thenReturn(true);

        // Verify the results
        assertTrue(userRepositoryUnderTest.createUserWithEmailAndPassword(email, "test123", u));

    }

    @Test
    public void testGetCurrentUser() {
        // Setup
        String email = "email@test.com";
        String name = "name";
        String surname = "surname";
        final User u = new User(name, surname, email);
        LiveData<User> userLiveData = new MutableLiveData<>(u);

        // Run the test
        when(userRepositoryUnderTest.getCurrentUser()).thenReturn(userLiveData);

        // Verify the results
        assertEquals(u, userRepositoryUnderTest.getCurrentUser().getValue());

    }

    @Test
    public void testSignOut() {
        // Setup

        // Run the test
        userRepositoryUnderTest.signOut();

        // Verify the results
    }

    @Test
    public void testGetCurrentUserUid() {
        // Setup

        // Run the test
        final LiveData<String> result = userRepositoryUnderTest.getCurrentUserUid();

        // Verify the results
    }

    @Test
    public void testGetInstance() {
        // Setup
        UserRepository first_instance = UserRepository.getInstance();
        UserRepository result = UserRepository.getInstance();

        // Check if both are equals
        assertEquals(first_instance, result);
    }
}
